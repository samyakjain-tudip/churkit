package com.churapplication

import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.ui.AppBarConfiguration
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.churapplication.R
import androidx.navigation.ui.NavigationUI
import com.churapplication.databinding.ActivityTrackingBinding
import com.churkit.Task.TrackerTask
import com.churkit.base.ChurKit
import com.churkit.services.ServiceAliveUtils
import com.google.android.material.snackbar.Snackbar

class TrackingActivity : AppCompatActivity() {
    private var appBarConfiguration: AppBarConfiguration? = null
    private var binding: ActivityTrackingBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTrackingBinding.inflate(layoutInflater)
        setContentView(binding!!.root)

        setSupportActionBar(binding!!.toolbar)


        OnTrackerServices()

        val navController =
            Navigation.findNavController(this, R.id.nav_host_fragment_content_tracking)
        appBarConfiguration = AppBarConfiguration.Builder(navController.graph).build()
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration!!)
        binding!!.fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController =
            Navigation.findNavController(this, R.id.nav_host_fragment_content_tracking)
        return (NavigationUI.navigateUp(navController, appBarConfiguration!!)
                || super.onSupportNavigateUp())
    }


    fun OnTrackerServices() {
        val tracker = TrackerTask.Builder()

            .build()
        tracker.serialId = ChurKit.getSerialNumber();
        if (ServiceAliveUtils.isServiceAlive("TrackerService")) {
            tracker.stop()
        } else {
            tracker.start()
        }
    }
}