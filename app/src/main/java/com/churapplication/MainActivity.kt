package com.churapplication

import android.Manifest
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat.OnRequestPermissionsResultCallback
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.google.android.gms.location.FusedLocationProviderClient
import android.os.Bundle
import com.mapbox.mapboxsdk.Mapbox
import com.churapplication.R
import com.mapbox.mapboxsdk.annotations.IconFactory
import android.content.Intent
import androidx.core.content.ContextCompat
import android.content.pm.PackageManager
import android.util.Log
import android.view.View
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import com.churapplication.MainActivity
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.OnSuccessListener
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.constants.Style
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.services.commons.models.Position


class MainActivity : AppCompatActivity(), OnRequestPermissionsResultCallback {
    private var mapView: MapView? = null
    private val map: MapboxMap? = null
    private var fusedLocationClient: FusedLocationProviderClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mapbox.getInstance(this, getString(R.string.mapbox_key))
        setContentView(R.layout.activity_main)
        mapView = findViewById<View>(R.id.mapView) as MapView
        mapView!!.onCreate(savedInstanceState);
        checkForUpdates()
     ///   mapView?.getMapboxMap()?.setStyle(Style.MAPBOX_STREETS)
        val icon =
            IconFactory.getInstance(this@MainActivity).fromResource(R.drawable.ic_directions_car)
        mapView!!.getMapAsync { mapboxMap ->
            // map = mapboxMap.get;
            mapboxMap.setStyle(Style.MAPBOX_STREETS)

            mapboxMap.setStyle(Style.MAPBOX_STREETS,
                MapboxMap.OnStyleLoadedListener {
                fun onStyleLoaded(@NonNull style: Style?) {
                    // Map is set up and the style has loaded. Now you can add data or make other map adjustments.
                }
            })

          /*  mapboxMap.addMarker(
                MarkerOptions()
                    .position(LatLng(23.8477294, 78.7293609))
                    .title("Stuttgart")
                    .snippet("Baden-Württemberg")
                    .icon(icon)
            )*/
            mapboxMap.onInfoWindowClickListener = MapboxMap.OnInfoWindowClickListener { marker ->
                val position = marker.position
                val location = ("https://www.google.com/maps?ll=" + position.latitude + ","
                        + position.longitude + "&q=" + position.latitude + ","
                        + position.longitude)
                val sendIntent = Intent(Intent.ACTION_SEND)
                sendIntent.putExtra(Intent.EXTRA_TEXT, location)
                sendIntent.type = "text/plain"
                startActivity(Intent.createChooser(sendIntent, "Send location"))
                true
            }
            mapboxMap.isMyLocationEnabled = true
            // Set the origin waypoint to the devices location
            // Set the origin waypoint to the devices location
            val origin: Position = Position.fromCoordinates(
                mapboxMap.myLocation!!.longitude,
                mapboxMap.myLocation!!.latitude
            )
            mapboxMap.addMarker(
                MarkerOptions()
                    .position(LatLng( mapboxMap.myLocation!!.latitude, mapboxMap.myLocation!!.longitude))
                    .title("My Current Location")
                    .snippet("My Current Location")
                    .icon(icon)
            )
            Log.i("RR", "mapboxMap.getMyLocation();$origin")
            // Log.i("RR","mapboxMap.getMyLocation();"+mapboxMap.getMyLocation().getLongitude());
            // Log.i("RR","mapboxMap.getMyLocation();"+mapboxMap.getMyLocation().getLongitude());
            mapboxMap.uiSettings.isZoomControlsEnabled = true
            mapboxMap.uiSettings.isZoomGesturesEnabled = true
            mapboxMap.uiSettings.isScrollGesturesEnabled = true
            mapboxMap.uiSettings.setAllGesturesEnabled(true)
            if (ContextCompat.checkSelfPermission(
                    this@MainActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                map?.setMyLocationEnabled(true);
            } else {
                //Request Location Permission
                ActivityCompat.requestPermissions(
                    this@MainActivity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    MY_PERMISSIONS_REQUEST_LOCATION
                )
            }
        }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)


    }

    public override fun onStart() {
        super.onStart()
        mapView!!.onStart()
    }

    public override fun onResume() {
        super.onResume()
        mapView!!.onResume()
        checkForCrashes()
    }

    public override fun onPause() {
        super.onPause()
        mapView!!.onPause()
        ///   unregisterManagers();
    }

    public override fun onStop() {
        super.onStop()
        mapView!!.onStop()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView!!.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView!!.onDestroy()
        //   unregisterManagers();
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView!!.onSaveInstanceState(outState)
    }

    private fun checkForCrashes() {
        //  CrashManager.register(this);
    }

    private fun checkForUpdates() {
        // Remove this for store builds!
        //   UpdateManager.register(this);
    }

    private fun unregisterManagers() {
        //UpdateManager.unregister();
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (permissions.size == 1 && Manifest.permission.ACCESS_FINE_LOCATION == permissions[0] && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                   map?.setMyLocationEnabled(true);
            }
        }
    }

    private fun shareLocation() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLocationClient!!.lastLocation
            .addOnSuccessListener(this) { location ->
                if (location != null) {
                    val locationUrl = ("https://www.google.com/maps?ll="
                            + location.latitude + "," + location.longitude
                            + "&q=" + location.latitude + "," + location.longitude)
                    val sendIntent = Intent(Intent.ACTION_SEND)
                    sendIntent.putExtra(Intent.EXTRA_TEXT, locationUrl)
                    sendIntent.type = "text/plain"
                    startActivity(Intent.createChooser(sendIntent, "Send location"))
                }
            }
    }

    companion object {
        const val MY_PERMISSIONS_REQUEST_LOCATION = 99
    }
}