package com.churapplication

import android.app.Application
import android.content.Context
import com.churkit.base.ChurKit.initialize
import com.churkit.base.ChurKit
import com.churkit.base.ChurKit.isDebugEnabled

class BaseApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        initialize(this, "c18716a1-1f04-441e-aa16-58af5caf7990", "uid2812")
        isDebugEnabled = true;
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
    }

}