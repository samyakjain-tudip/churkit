package com.churkit.model.response

import com.google.gson.annotations.SerializedName

class CKRegisterUserResponse(code: Int?, serial: String?, msg: String?) {
    @SerializedName("code")
    var code: Int? = null

    @SerializedName("serial")
    var serial: String? = null

    @SerializedName("msg")
    var msg: String? = null
}