package com.churkit.model.response

import com.churkit.model.response.CKBeaconlistResponse.Beaconsbean
import com.google.gson.annotations.SerializedName

class CKBeaconlistResponse {
    @SerializedName("message")
    private val message: Int? = null

    @SerializedName("msg")
    private val msg: String? = null

    @SerializedName("beacons")
    private val beacons: List<Beaconsbean>? = null

    class Beaconsbean {
        @SerializedName("id")
        private val id: Int? = null

        @SerializedName("floor_plan_id")
        private val floorPlanId: String? = null

        @SerializedName("uuid")
        private val uuid: String? = null

        @SerializedName("major")
        private val major: String? = null

        @SerializedName("minor")
        private val minor: String? = null

        @SerializedName("name")
        private val name: String? = null

        @SerializedName("lat")
        private val lat: Double? = null

        @SerializedName("lng")
        private val lng: Double? = null

        @SerializedName("business_id")
        private val businessId: String? = null

    }

}