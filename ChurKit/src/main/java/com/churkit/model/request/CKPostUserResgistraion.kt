package com.churkit.model.request

import com.google.gson.annotations.SerializedName

class CKPostUserResgistraion( @SerializedName("app_id") private var appId: String, @SerializedName("type") private var  type: String,@SerializedName("business_user_id") private var businessUserId: String)  {

    @SerializedName("first_name")
    var firstName: String? = null

    @SerializedName("last_name")
    var lastName: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("avatar")
    var avatar: String? = null

    @SerializedName("birthday")
    var birthday: String? = null

    @SerializedName("gender")
    var gender: String? = null

    @SerializedName("profession")
    var profession: String? = null

    @SerializedName("city")
    var city: String? = null

    @SerializedName("country")
    var country: String? = null
}