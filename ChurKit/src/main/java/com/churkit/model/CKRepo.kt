package com.churkit.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Samyak Jain on 22,Nov,2021
 */

class CKRepo(id: Long, name: String, description: String, owner: CKUser, stars: Long, forks: Long) {
    val id: Long
    val name: String
    val description: String
    val owner: CKUser

    @SerializedName("stargazers_count")
    val stars: Long

    @SerializedName("forks_count")
    val forks: Long

    init {
        this.id = id
        this.name = name
        this.description = description
        this.owner = owner
        this.stars = stars
        this.forks = forks
    }
}