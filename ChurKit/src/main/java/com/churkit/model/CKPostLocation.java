package com.churkit.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class CKPostLocation {
    public String uid;
    public String lat;
    public String lang;
    public String address;


    public CKPostLocation() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public CKPostLocation(String uid, String lat,String lang, String address) {
        this.uid = uid;
        this.lat = lat;
        this.lang = lang;
        this.address = address;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", uid);
        result.put("lang", lang);
        result.put("lat", lat);
        result.put("address", address);
        return result;
    }
}