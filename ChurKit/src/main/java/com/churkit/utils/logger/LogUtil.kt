/*
 * Copyright © 2019, Chur Networks Pty Ltd - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package com.churkit.utils.logger

import android.util.Log
import com.churkit.base.ChurKit.isDebugEnabled
import com.churkit.base.ChurKit
import kotlin.jvm.JvmOverloads

object LogUtil {
    var isDEBUG = isDebugEnabled
    fun v(tag: String?, msg: String?) {
        if (isDEBUG) {
            Log.v(tag, msg!!)
        }
    }

    @JvmOverloads
    fun d(tag: String?, msg: String?, tr: Throwable? = null) {
        if (isDEBUG) {
            Log.d(tag, msg, tr)
        }
    }

    fun i(tag: String?, msg: String?) {
        if (isDEBUG) {
            Log.i(tag, msg!!)
        }
    }

    fun w(tag: String?, msg: String?) {
        if (isDEBUG) {
            Log.w(tag, msg!!)
        }
    }

    @JvmOverloads
    fun e(tag: String?, msg: String?, tr: Throwable? = null) {
        if (isDEBUG) {
            Log.e(tag, msg, tr)
        }
    }

    fun println(str: String?) {
        if (isDEBUG) {
            println(str)
        }
    }
}