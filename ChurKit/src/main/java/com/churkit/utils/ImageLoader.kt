package com.churkit.utils

import android.content.Context
import com.churkit.utils.logger.LogUtil.i
import com.churkit.utils.ImageLoader.OnBitmapLoadCallBack
import android.graphics.Bitmap
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.target.Target


/**
 * Created by Samyak Jain on 07,Dec,2021
 */

object ImageLoader {
    fun getBitmap(context: Context?, url: String?, callBack: OnBitmapLoadCallBack): Bitmap? {
        if (context == null) return null
        Glide.with(context)
            .asBitmap()
            .load(url)
            .listener(object : RequestListener<Bitmap> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any,
                    target: Target<Bitmap>,
                    isFirstResource: Boolean
                ): Boolean {
                    i("Glide", e!!.message)
                    return false
                }

                override fun onResourceReady(
                    resource: Bitmap,
                    model: Any,
                    target: Target<Bitmap>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    i(
                        "Glide",
                        resource.toString()
                    )
                    callBack.onLoad(resource)
                    return false
                }
            })
            .submit()
        return null
    }

    interface OnBitmapLoadCallBack {
        fun onLoad(bitmap: Bitmap?)
    }
}