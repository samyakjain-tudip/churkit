package com.churkit.utils

import android.content.Context
import android.widget.Toast
import android.text.Html
import android.view.Gravity
import java.lang.Exception
import java.lang.NumberFormatException

object CKAppUtill {
    fun isStringNullOrBlank(str: String?): Boolean {
        if (str == null) {
            return true
        } else if (str == "null" || str.trim { it <= ' ' } == "") {
            return true
        }
        return false
    }

    fun isNumeric(str: String): Boolean {
        try {
            val d = str.toDouble()
        } catch (nfe: NumberFormatException) {
            return false
        }
        return true
    }

    fun isStringNull(id: Int): Boolean {
        return id == 0
    }

    fun showToast(message: String?, ctx: Context?) {
        try {
            var toast: Toast? = null
            if (ctx != null) toast = Toast.makeText(ctx, Html.fromHtml(message), Toast.LENGTH_LONG)
            //toast.setGravity(Gravity.BOTTOM, 0, 0);
            toast!!.setGravity(Gravity.CENTER, 20, 20)
            toast.show()
            return
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}