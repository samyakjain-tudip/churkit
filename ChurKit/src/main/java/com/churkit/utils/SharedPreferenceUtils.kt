package com.churkit.utils

import android.content.Context
import android.content.SharedPreferences
import com.churkit.utils.SharedPreferenceUtils

/**
 * Created by Prefme on 17/10/2018.
 */
class SharedPreferenceUtils private constructor(context: Context) {
    var sharedPreferences: SharedPreferences
    private val logindetaile: SharedPreferences
    fun getString(key: String?): String? {
        return sharedPreferences.getString(key, null)
    }

    fun getString(key: String?, defVal: String?): String? {
        return sharedPreferences.getString(key, defVal)
    }

    fun getPrefrenceObject(key: String?, defVal: String?): String? {
        return sharedPreferences.getString(key, defVal)
    }

    fun getInt(key: String?): Int {
        return sharedPreferences.getInt(key, 0)
    }

    fun getInt(key: String?, defVal: Int): Int {
        return sharedPreferences.getInt(key, defVal)
    }

    fun saveInt(key: String?, value: Int) {
        val editor = sharedPreferences.edit()
        editor.putInt(key, value)
        editor.commit()
    }

    fun getBoolean(key: String?): Boolean {
        return sharedPreferences.getBoolean(key, false)
    }

    fun getBoolean(key: String?, defVal: Boolean): Boolean {
        return sharedPreferences.getBoolean(key, defVal)
    }

    fun saveBoolean(key: String?, value: Boolean) {
        val editor = sharedPreferences.edit()
        editor.putBoolean(key, value)
        editor.commit()
    }
    fun saveString(key: String?, value: String) {
        val editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.commit()
    }
    fun getFloat(key: String?): Float {
        return sharedPreferences.getFloat(key, 0f)
    }

    fun getFloat(key: String?, defVal: Float): Float {
        return sharedPreferences.getFloat(key, defVal)
    }

    fun saveFloat(key: String?, value: Float) {
        val editor = sharedPreferences.edit()
        editor.putFloat(key, value)
        editor.commit()
    }

    /**
     * Clear all values from this preference
     */
    fun clear() {
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.commit()
    }

    /**
     * Clear value of given key from this preference
     *
     * @param key name of the key whose value to be removed
     */
    fun clear(key: String?) {
        val editor = sharedPreferences.edit()
        editor.remove(key)
        editor.commit()
    }

    companion object {
        private var PREFERENCE_NAME = "SDK_CRED"
        private var PREFERENCE_login = "USER_CRED"
        var sharedPreferenceUtils: SharedPreferenceUtils? = null
        var sharedPreferenceUtilsSettimng: SharedPreferenceUtils? = null

        /**
         * It returns singleton object of @[SharedPreferenceUtils] class. The best practice is to create its instance once in your MyApp class and use it everywhere so that you don't need to pass context again.
         *
         * @param context
         * @return Instance of @[SharedPreferenceUtils] class
         */
        fun getInstance(context: Context): SharedPreferenceUtils? {
            if (sharedPreferenceUtils == null) {
                sharedPreferenceUtils = SharedPreferenceUtils(context)
            }
            return sharedPreferenceUtils
        }
    }

    init {
        PREFERENCE_NAME = PREFERENCE_NAME + context.packageName
        PREFERENCE_login = PREFERENCE_login + context.packageName
        sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
        logindetaile = context.getSharedPreferences(PREFERENCE_login, Context.MODE_PRIVATE)
    }
}