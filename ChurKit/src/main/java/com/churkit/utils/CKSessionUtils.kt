package com.churkit.utils

import android.content.Context
import com.churkit.utils.SharedPreferenceUtils
import com.churkit.utils.CKSessionUtils

class CKSessionUtils private constructor(private val mContext: Context) {
    private val mPrefs: SharedPreferenceUtils
    var isWifiPositioningEnabled: Boolean
        get() = mPrefs.getBoolean(KEY_WIFI_POSITIONING_ENABLE, true)
        set(isEnabled) {
            mPrefs.sharedPreferences.edit()
                .putBoolean(KEY_WIFI_POSITIONING_ENABLE, isEnabled)
                .apply()
        }
    var isNativePositioningEnabled: Boolean
        get() = mPrefs.getBoolean(KEY_NATIVE_POSITIONING_ENABLE, true)
        set(isEnabled) {
            mPrefs.sharedPreferences.edit()
                .putBoolean(KEY_NATIVE_POSITIONING_ENABLE, isEnabled)
                .apply()
        }
    var isPDRPositioningEnabled: Boolean
        get() = mPrefs.getBoolean(KEY_PDR_POSITIONING_ENABLE, true)
        set(isEnabled) {
            mPrefs.sharedPreferences.edit()
                .putBoolean(KEY_PDR_POSITIONING_ENABLE, isEnabled)
                .apply()
        }
    var floorHeight: Int
        get() = mPrefs.getInt(KEY_HEIGHT, 3)
        set(height) {
            mPrefs.sharedPreferences.edit()
                .putInt(KEY_HEIGHT, height)
                .apply()
        }
    var floorPlanId: String?
        get() = mPrefs.getString(KEY_FLOOR_PLAN_ID, null)
        set(floorPlanId) {
            mPrefs.sharedPreferences.edit()
                .putString(KEY_FLOOR_PLAN_ID, floorPlanId)
                .apply()
        }
    var locationId: String?
        get() = mPrefs.getString(KEY_LOCATION_ID, null)
        set(locationId) {
            mPrefs.sharedPreferences.edit()
                .putString(KEY_LOCATION_ID, locationId)
                .apply()
        }
    var buildingLocation: String?
        get() = mPrefs.getString(KEY_BUILDING_LOCATION, "0 0")
        set(location) {
            mPrefs.sharedPreferences.edit()
                .putString(KEY_BUILDING_LOCATION, location)
                .apply()
        }
    var buildingRadius: String?
        get() = mPrefs.getString(KEY_BUILDING_RADIUS, "100")
        set(radius) {
            mPrefs.sharedPreferences.edit().putString(KEY_BUILDING_RADIUS, radius)
                .apply()
        }
    var key: String?
        get() = mPrefs.getString(KEY_SDK_KEY, null)
        set(key) {
            mPrefs.sharedPreferences.edit().putString(KEY_SDK_KEY, key)
                .apply()
        }
    var secret: String?
        get() = mPrefs.getString(KEY_SDK_SECRET, null)
        set(secret) {
            mPrefs.sharedPreferences.edit().putString(KEY_SDK_SECRET, secret)
                .apply()
        }
    var businessUserId: String?
        get() = mPrefs.getString(KEY_USER_ID, null)
        set(userId) {
            mPrefs.sharedPreferences.edit().putString(KEY_USER_ID, userId)
                .apply()
        }
    var businessId: String?
        get() = mPrefs.getString(KEY_BUSINESS_ID, null)
        set(businessId) {
            mPrefs.sharedPreferences.edit().putString(KEY_BUSINESS_ID, businessId)
                .apply()
        }
    var serialId: String?
        get() = mPrefs.getString(KEY_SERIAL_ID, null)
        set(serialId) {
            mPrefs.sharedPreferences.edit().putString(KEY_SERIAL_ID, serialId)
                .apply()
        }

    var username: String?
        get() = mPrefs.getString(KEY_USERNAME, null)
        set(username) {
            mPrefs.sharedPreferences.edit().putString(KEY_USERNAME, username)
                .apply()
        }


    companion object {
        private const val MULTI_PROCESS_FILE_LOCATION_NAME = "chur_multi_process_prefs_sdk"
        private const val KEY_WIFI_POSITIONING_ENABLE = "key_wifi"
        private const val KEY_NATIVE_POSITIONING_ENABLE = "key_native"
        private const val KEY_PDR_POSITIONING_ENABLE = "key_pdr"
        private const val KEY_HEIGHT = "key_height"
        private const val KEY_FLOOR_PLAN_ID = "key_floor_plan_id"
        private const val KEY_LOCATION_ID = "key_location_id"
        private const val KEY_BUILDING_LOCATION = "key_building_location"
        private const val KEY_BUILDING_RADIUS = "key_building_radius"
        private const val KEY_SDK_KEY = "key_sdk_key"
        private const val KEY_SDK_SECRET = "key_sdk_secret"
        private const val KEY_USER_ID = "key_user_id"
        private const val KEY_BUSINESS_ID = "key_business_id"
        const val KEY_SERIAL_ID = "key_serial_id"
        private const val KEY_USERNAME = "username"
        var instance: CKSessionUtils? = null
            private set

        fun initHelper(context: Context): CKSessionUtils? {
            if (instance == null) {
                instance = CKSessionUtils(context)
            }
            return instance
        }
    }

    init {
        mPrefs = SharedPreferenceUtils.getInstance(mContext)!!
    }
}