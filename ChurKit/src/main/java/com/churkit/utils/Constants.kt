package com.churkit.utils

object Constants {
    //Geofence
    const val GEOFENCE_LOITERING_DELAY_IN_MILLISECONDS = 60 * 1000 // 1min

    //Notification
    const val NOTIFICATION_TITLE_FOREGROUND = "notificationTitleForeground"

    const val NOTIFICATION_CONTENT_FOREGROUND = "notificationContentForeground"
}