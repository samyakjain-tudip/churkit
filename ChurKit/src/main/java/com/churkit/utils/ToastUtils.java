/*
 * Copyright © 2019, Chur Networks Pty Ltd - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.churkit.utils;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import androidx.annotation.StringRes;



public class ToastUtils {
    private ToastUtils() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    private static Toast sToast;
    private static Handler sHandler = new Handler(Looper.getMainLooper());
    private static boolean isJumpWhenMore = true;

    /**
     * Toast init
     *
     * @param isJumpWhenMore <p>{@code true}: pop new toast <br>{@code false}: change text only </p>
     *                       <p>If {@code false} show long or short toast</p>
     */
    public static void init(boolean isJumpWhenMore) {
        ToastUtils.isJumpWhenMore = isJumpWhenMore;
    }


    public static void showShortToastSafe(Context mContext,final CharSequence text) {
        sHandler.post(new Runnable() {
            @Override
            public void run() {
                showToast(mContext,text, Toast.LENGTH_SHORT);
            }
        });
    }

    public static void showShortToastSafe(Context mContext,final @StringRes int resId) {
        sHandler.post(new Runnable() {
            @Override
            public void run() {
                showToast(mContext,resId, Toast.LENGTH_SHORT);
            }
        });
    }


    public static void showShortToastSafe(Context mContext,final @StringRes int resId, final Object... args) {
        sHandler.post(new Runnable() {
            @Override
            public void run() {
                showToast(mContext,resId, Toast.LENGTH_SHORT, args);
            }
        });
    }

    public static void showShortToastSafe(Context mContext,final String format, final Object... args) {
        sHandler.post(new Runnable() {
            @Override
            public void run() {
                showToast(mContext,format, Toast.LENGTH_SHORT, args);
            }
        });
    }

    public static void showLongToastSafe(Context mContext,final CharSequence text) {
        sHandler.post(new Runnable() {
            @Override
            public void run() {
                showToast(mContext,text, Toast.LENGTH_LONG);
            }
        });
    }

    public static void showLongToastSafe(Context mContext,final @StringRes int resId) {
        sHandler.post(new Runnable() {
            @Override
            public void run() {
                showToast(mContext,resId, Toast.LENGTH_LONG);
            }
        });
    }

    public static void showLongToastSafe(Context mContext,final @StringRes int resId, final Object... args) {
        sHandler.post(new Runnable() {
            @Override
            public void run() {
                showToast( mContext,resId, Toast.LENGTH_LONG, args);
            }
        });
    }

    public static void showLongToastSafe(Context mContext,final String format, final Object... args) {
        sHandler.post(new Runnable() {
            @Override
            public void run() {
                showToast(mContext,format, Toast.LENGTH_LONG, args);
            }
        });
    }

    public static void showShortToast(Context mContext,CharSequence text) {
        showToast(mContext,text, Toast.LENGTH_SHORT);
    }

    public static void showShortToast(Context mContext,@StringRes int resId) {
        showToast(mContext,resId, Toast.LENGTH_SHORT);
    }

    public static void showShortToast(Context mContext,@StringRes int resId, Object... args) {
        showToast(mContext,resId, Toast.LENGTH_SHORT, args);
    }

    public static void showShortToast(Context mContext,String format, Object... args) {
        showToast(mContext,format, Toast.LENGTH_SHORT, args);
    }

    public static void showLongToast(Context mContext,CharSequence text) {
        showToast(mContext,text, Toast.LENGTH_LONG);
    }

    public static void showLongToast(Context mContext,@StringRes int resId) {
        showToast(mContext,resId, Toast.LENGTH_LONG);
    }

    public static void showLongToast(Context mContext,@StringRes int resId, Object... args) {
        showToast(mContext,resId, Toast.LENGTH_LONG, args);
    }

    public static void showLongToast(Context mContext,String format, Object... args) {
        showToast(mContext,format, Toast.LENGTH_LONG, args);
    }

    private static void showToast(Context mContext,@StringRes int resId, int duration) {
        showToast(mContext,mContext.getApplicationContext().getResources().getText(resId).toString(), duration);
    }

    private static void showToast(Context mContext,@StringRes int resId, int duration, Object... args) {
        showToast(mContext,String.format(mContext.getApplicationContext().getResources().getString(resId), args), duration);
    }

    private static void showToast(Context mContext,String format, int duration, Object... args) {
        showToast(mContext,String.format(format, args), duration);
    }

    private static void showToast(Context mContext,CharSequence text, int duration) {
        if (isJumpWhenMore) cancelToast();
        if (sToast == null) {
            sToast = Toast.makeText(mContext, text, duration);
        } else {
            sToast.setText(text);
            sToast.setDuration(duration);
        }
        sToast.show();
    }

    private static void cancelToast() {
        if (sToast != null) {
            sToast.cancel();
            sToast = null;
        }
    }
}
