package com.churkit.utils

import android.app.ActivityManager
import android.content.Context

/**
 * Created by Samyak Jain on 07,Dec,2021
 */

object ServiceUtils {
    fun isServiceAlive(className: String, context: Context): Boolean {
        var isServiceRunning = false
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            ?: return true
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (className == service.service.className) {
                isServiceRunning = true
            }
        }
        return isServiceRunning
    }
}