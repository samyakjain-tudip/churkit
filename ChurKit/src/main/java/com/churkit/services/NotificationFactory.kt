package com.churkit.services

import android.app.Notification
import com.churkit.services.NotificationFactory
import android.os.Build
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import androidx.core.app.NotificationCompat
import com.churkit.R

object NotificationFactory {
    const val CHANNEL_ID_FOREGROUND_SERVICE = 1109
    private var serviceNotification: Notification? = null
    fun getServiceNotification(context: Context, title: String?, content: String?): Notification? {
        if (serviceNotification == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val importance = NotificationManager.IMPORTANCE_HIGH
                val channel = NotificationChannel(
                    Integer.toString(CHANNEL_ID_FOREGROUND_SERVICE),
                    "Tracker Service Channel", importance
                )
                channel.description = "Tracker Service Channel"
                channel.setSound(null, null)

                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this
                val notificationManager = context.applicationContext.getSystemService(
                    NotificationManager::class.java
                )
                notificationManager.createNotificationChannel(channel)
            }
            val builder = NotificationCompat.Builder(
                context, Integer.toString(
                    CHANNEL_ID_FOREGROUND_SERVICE
                )
            )
                .setContentTitle(title)
                .setContentText(content)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder.setSmallIcon(R.drawable.ic_launcher_background)
            }
            serviceNotification = builder.build()
        }
        return serviceNotification
    }
}