package com.churkit.services

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.TaskStackBuilder
import com.churkit.R
import com.churkit.model.CKPostLocation
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.mapbox.mapboxsdk.geometry.LatLng
import java.util.*

class CKUpdateDataService : Service() {
    private var mLocationManager: LocationManager? = null

    var notificationMessage: String? = null
    var groupname: String? = null
    var endlat = 0.0
    var endlang = 0.0
    private val isEndTrackingNotRunningRightNow = true
    var mDatabase: DatabaseReference? = null
    //    private int defaultGeoFencingRadarDestinationExactlyNear = 10;
    private val defaultGeoFencingRadarDestination = 50
    private val defaultGeoFencingRadarLandmark = 100

    //    private Handler locationUpdatehandler;
    var mContext: Context? = null

    inner class LocationListener(provider: String) : android.location.LocationListener {
        var mLastLocation: Location
        override fun onLocationChanged(location: Location) {
            Log.e(TAG, "onLocationChanged: $location")
            mLastLocation.set(location)
            try {
            } catch (e: Exception) {
                e.printStackTrace()
            }
            val latitude = location.latitude
            val longitude = location.longitude
            val latLng = LatLng(latitude, longitude)
            //  sourceMarker.setPosition(latLng);
            //   movement(latLng);
            val endPoint = Location("locationA")
            endPoint.latitude = endlat
            endPoint.longitude = endlang
            val distance = location.distanceTo(endPoint).toDouble()

            writeCKPostLocation("101",""+endlat,""+endlang,"Test Address")
        }

        override fun onProviderDisabled(provider: String) {
            Log.e(TAG, "onProviderDisabled: $provider")
        }

        override fun onProviderEnabled(provider: String) {
            Log.e(TAG, "onProviderEnabled: $provider")
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
            Log.e(TAG, "onStatusChanged: $provider")
        }

        init {
            Log.e(TAG, "LocationListener $provider")
            mLastLocation = Location(provider)
        }
    }

    var mLocationListeners = arrayOf<LocationListener>(
        LocationListener(LocationManager.GPS_PROVIDER),
        LocationListener(LocationManager.NETWORK_PROVIDER)
    )

    /*  LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.PASSIVE_PROVIDER)
    };*/
    override fun onBind(arg0: Intent): IBinder? {
        return null
    }

    private var mainIntent: Intent? = null
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.e(TAG, "onStartCommand")
        super.onStartCommand(intent, flags, startId)
        mainIntent = intent
        setAllValuesFromIntent(intent)
        if (intent != null) {
            val action = intent.action
            if (action != null) when (action) {
                ACTION_START_FOREGROUND_SERVICE -> {
                    startForegroundService()
                    val `in` = Intent()
                    `in`.putExtra("type", 1)
                    sendBroadcast(`in`)
                    Toast.makeText(
                        applicationContext,
                        "Foreground service is started.",
                        Toast.LENGTH_LONG
                    ).show()
                }
                ACTION_START_BACKROUND_SERVICE -> {
                    startForegroundService()
                    val `in` = Intent()
                    `in`.putExtra("type", 1)
                    sendBroadcast(`in`)
                    Toast.makeText(
                        applicationContext,
                        "Foreground service is started.",
                        Toast.LENGTH_LONG
                    ).show()
                }
                ACTION_STOP_FOREGROUND_SERVICE -> {
                    stopForegroundService()
                    val in1 = Intent()
                    in1.putExtra("type", 0)
                    sendBroadcast(in1)
                    Toast.makeText(
                        applicationContext,
                        "Foreground service is stopped.",
                        Toast.LENGTH_LONG
                    ).show()
                }
                ACTION_PLAY -> Toast.makeText(
                    applicationContext,
                    "You click Play button.",
                    Toast.LENGTH_LONG
                ).show()
                ACTION_PAUSE -> Toast.makeText(
                    applicationContext,
                    "You click Pause button.",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
        return START_STICKY
    }

    private fun setAllValuesFromIntent(intent: Intent) {
        try {
            if (intent.hasExtra("endlat")) {
                endlat = intent.getDoubleExtra("endlat", 0.0)
            }
            if (intent.hasExtra("endlang")) {
                endlang = intent.getDoubleExtra("endlang", 0.0)
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun stopForegroundService() {
        try {
            Log.d(TAG_FOREGROUND_SERVICE, "Stop foreground service.")
            // Stop foreground service and remove the notification.
            stopForeground(true)
            // Stop the foreground service.
            stopSelf()
        } catch (ae: Exception) {
            Toast.makeText(mContext, "" + ae, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreate() {
        Log.e(TAG, "onCreate")
        mContext = this
        mDatabase = FirebaseDatabase.getInstance().reference
        initializeLocationManager()
    }

    override fun onDestroy() {
        Log.e(TAG, "onDestroy")
        super.onDestroy()
    }

    private fun initializeLocationManager() {
        Log.e(
            TAG,
            "initializeLocationManager - LOCATION_INTERVAL: " + LOCATION_INTERVAL + " LOCATION_DISTANCE: " + LOCATION_DISTANCE
        )
        if (mLocationManager == null) {
            mLocationManager =
                applicationContext.getSystemService(LOCATION_SERVICE) as LocationManager
        }
    }

    private fun startForegroundService() {
        Log.d(TAG_FOREGROUND_SERVICE, "Start foreground service.")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel("my_service", "My Background Service")
        } else {

            // Create notification default intent.
            val intent = Intent()
            val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

            // Create notification builder.
            val builder = NotificationCompat.Builder(this)

            // Make notification show big text.
            val bigTextStyle = NotificationCompat.BigTextStyle()
            bigTextStyle.setBigContentTitle("Pataa")
            bigTextStyle.bigText("" + notificationMessage)
            // Set big text style.
            builder.setStyle(bigTextStyle)
            builder.setWhen(System.currentTimeMillis())
            builder.setSmallIcon(notificationIcon)
            val largeIconBitmap =
                BitmapFactory.decodeResource(resources, R.drawable.ic_launcher_background)
            builder.setLargeIcon(largeIconBitmap)
            // Make the notification max priority.
            builder.priority = Notification.PRIORITY_MAX
            // Make head-up notification.
            builder.setFullScreenIntent(pendingIntent, true)

            // Add Play button intent in notification.
            val playIntent = Intent(this, CKUpdateDataService::class.java)
            playIntent.action = ACTION_PLAY
            val pendingPlayIntent = PendingIntent.getService(this, 0, playIntent, 0)
            val playAction = NotificationCompat.Action(
                android.R.drawable.ic_media_play,
                "Play",
                pendingPlayIntent
            )
            builder.addAction(playAction)

            // Add Pause button intent in notification.
            val pauseIntent = Intent(this, CKUpdateDataService::class.java)
            pauseIntent.action = ACTION_PAUSE
            val pendingPrevIntent = PendingIntent.getService(this, 0, pauseIntent, 0)
            val prevAction = NotificationCompat.Action(
                android.R.drawable.ic_media_pause,
                "Pause",
                pendingPrevIntent
            )
            builder.addAction(prevAction)

            // Build the notification.
            val notification = builder.build()

            // Start foreground service.
            startForeground(1, notification)
        }
    }

    private val notificationIcon: Int
        private get() {
            val useWhiteIcon = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
            return if (useWhiteIcon) R.drawable.ic_launcher_background else R.drawable.ic_launcher_background
        }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String) {
//        Intent resultIntent = new Intent(this, SplashMainActivity.class);
// Create the TaskStackBuilder and add the intent, which inflates the back stack
        val stackBuilder = TaskStackBuilder.create(this)
        stackBuilder.addNextIntentWithParentStack(Intent())
        val resultPendingIntent =
            stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        val channel_ =
            NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT)
        channel_.lightColor = Color.BLUE
        channel_.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val manager = (getSystemService(NOTIFICATION_SERVICE) as NotificationManager)
        if (channel_.canShowBadge()) {
            // do something when dot is enabled
            channel_.setShowBadge(false)
        } else if (!channel_.canShowBadge()) {
            println(".......... dot is disabled") // do something when dot is disabled
        }
        manager.createNotificationChannel(channel_)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
        val notification = notificationBuilder.setOngoing(true)
            .setSmallIcon(notificationIcon)
            .setContentTitle("" + notificationMessage)
            .setPriority(NotificationManager.IMPORTANCE_MIN)
            .setCategory(Notification.CATEGORY_SERVICE)
            .setNumber(0)
            .setContentIntent(resultPendingIntent) //intent
            .build()
        val notificationManager = NotificationManagerCompat.from(this)
        notificationManager.notify(1, notificationBuilder.build())
        startForeground(1, notification)
    }

    protected val deleteIntent: PendingIntent
        protected get() = PendingIntent.getBroadcast(
            mContext,
            0,
            Intent("detectNotificationDeleteBroadcast"),
            PendingIntent.FLAG_CANCEL_CURRENT
        )

    companion object {
        private const val TAG = "CKUpdateDataService"
        private const val LOCATION_INTERVAL = 1000
        private const val LOCATION_DISTANCE = 5f
        private const val TAG_FOREGROUND_SERVICE = "FOREGROUND_SERVICE"
        const val ACTION_START_FOREGROUND_SERVICE = "ACTION_START_FOREGROUND_SERVICE"
        const val ACTION_STOP_FOREGROUND_SERVICE = "ACTION_STOP_FOREGROUND_SERVICE"
        const val ACTION_START_BACKROUND_SERVICE = "ACTION_START_BACKGROUND_SERVICE"
        const val ACTION_PAUSE = "ACTION_PAUSE"
        const val ACTION_PLAY = "ACTION_PLAY"
        var type = 0
    }

    private fun writeCKPostLocation(userId: String, lat: String, lang: String, address: String) {
        // Create new post at /user-posts/$userid/$postid
        // and at /posts/$postid simultaneously
        val key = mDatabase!!.child("CKPostLocation").push().key
        val post = CKPostLocation(userId, lat, lang, address)
        val postValues: Map<String, Any> = post.toMap()
        val childUpdates: MutableMap<String, Any> = HashMap()
        childUpdates["/posts/$key"] = postValues
        childUpdates["/user-posts/$userId/$key"] = postValues
        mDatabase!!.updateChildren(childUpdates)
    }
}