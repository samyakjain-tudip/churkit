package com.churkit.services

import com.churkit.base.ChurKit.getAppSDKInstance
import android.app.ActivityManager
import android.content.Context
import com.churkit.base.ChurKit

object ServiceAliveUtils {
    @JvmStatic
    fun isServiceAlive(className: String): Boolean {
        var isServiceRunning = false
        val manager =
            getAppSDKInstance()!!.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
                ?: return true
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (className == service.service.className) {
                isServiceRunning = true
            }
        }
        return isServiceRunning
    }
}