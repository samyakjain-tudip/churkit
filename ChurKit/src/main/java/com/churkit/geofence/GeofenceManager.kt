package com.churkit.geofence

import com.churkit.geofence.GeofenceErrorMessages.getErrorString
import com.churkit.utils.logger.LogUtil.w
import android.app.PendingIntent
import com.churkit.geofence.GeofenceManager.PendingGeofenceTask
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import com.churkit.geofence.GeofenceBroadcastReceiver
import com.churkit.geofence.GeofenceErrorMessages
import com.churkit.geofence.GeofenceManager
import com.churkit.utils.Constants
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingClient
import com.google.android.gms.location.GeofencingRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import java.util.ArrayList

class GeofenceManager private constructor(context: Context) : OnCompleteListener<Void?> {
    /**
     * Tracks whether the user requested to add or remove geofences, or to do neither.
     */
    private enum class PendingGeofenceTask {
        ADD, REMOVE, NONE
    }

    private val mGeofencingClient: GeofencingClient
    private var mGeofencePendingIntent: PendingIntent? = null
    private val mContext: Context
    private val mGeofenceList: ArrayList<Geofence>?
    private var mPendingGeofenceTask = PendingGeofenceTask.NONE
    @Synchronized
    fun initGeofences(geofencId: String?, lat: Double, lng: Double, radius: Float) {
        mGeofenceList!!.clear()
        mGeofenceList.add(
            Geofence.Builder() // Set the request ID of the geofence. This is a string to identify this
                // geofence.
                .setRequestId(geofencId) // Set the circular region of this geofence.
                //radius unit meters
                .setCircularRegion(
                    lat, lng,
                    radius
                ) // Set the expiration duration of the geofence. This geofence gets automatically
                // removed after this period of time.
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setLoiteringDelay(Constants.GEOFENCE_LOITERING_DELAY_IN_MILLISECONDS) // Set the transition types of interest. Alerts are only generated for these
                // transition. We track entry and exit transitions in this sample.
                .setTransitionTypes(
                    Geofence.GEOFENCE_TRANSITION_ENTER or
                            Geofence.GEOFENCE_TRANSITION_EXIT or Geofence.GEOFENCE_TRANSITION_DWELL
                ) // Create the geofence.
                .build()
        )
        addGeofences()
    }

    @SuppressLint("MissingPermission")
    private fun addGeofences() {
        mGeofencingClient.addGeofences(geofencingRequest, geofencePendingIntent)
            .addOnCompleteListener(this)
        mPendingGeofenceTask = PendingGeofenceTask.ADD
    }

    fun removeGeofences() {
        mGeofenceList?.clear()
        mGeofencingClient.removeGeofences(geofencePendingIntent).addOnCompleteListener(this)
        mPendingGeofenceTask = PendingGeofenceTask.REMOVE
    }
    // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
    // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
    // is already inside that geofence.

    // Add the geofences to be monitored by geofencing service.

    // Return a GeofencingRequest.
    @get:Synchronized
    private val geofencingRequest: GeofencingRequest
        private get() {
            val builder = GeofencingRequest.Builder()

            // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
            // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
            // is already inside that geofence.
            builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER or GeofencingRequest.INITIAL_TRIGGER_DWELL)

            // Add the geofences to be monitored by geofencing service.
            builder.addGeofences(mGeofenceList)

            // Return a GeofencingRequest.
            return builder.build()
        }

    // Reuse the PendingIntent if we already have it.
    // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
    // addGeofences() and removeGeofences().
    private val geofencePendingIntent: PendingIntent?
        private get() {
            // Reuse the PendingIntent if we already have it.
            if (mGeofencePendingIntent != null) {
                return mGeofencePendingIntent
            }
            val intent = Intent(mContext, GeofenceBroadcastReceiver::class.java)
            // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
            // addGeofences() and removeGeofences().
            mGeofencePendingIntent =
                PendingIntent.getBroadcast(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            return mGeofencePendingIntent
        }

    override fun onComplete(task: Task<Void?>) {
        if (task.isSuccessful) {

//            ToastUtils.showShortToastSafe("Added");
        } else {
            // Get the status code for the error and log it using a user-friendly message.
            val errorMessage = getErrorString(mContext, task.exception)
            //      ToastUtils.showShortToast(errorMessage);
            w(TAG, errorMessage)
        }
    }

    companion object {
        private val TAG = GeofenceManager::class.java.canonicalName
        private var instance: GeofenceManager? = null
        fun getInstacne(context: Context): GeofenceManager? {
            synchronized(GeofenceManager::class.java) {
                if (instance == null) {
                    instance = GeofenceManager(context)
                }
            }
            return instance
        }
    }

    init {
        mGeofencingClient = LocationServices.getGeofencingClient(context)
        mContext = context
        mGeofenceList = ArrayList()
    }
}