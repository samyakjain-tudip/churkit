package com.churkit.geofence

import android.content.Context
import com.churkit.geofence.GeofenceErrorMessages.getErrorString
import androidx.core.app.JobIntentService
import android.content.Intent
import android.text.TextUtils
import com.churkit.geofence.GeofenceErrorMessages
import com.churkit.R
import com.churkit.geofence.GeofenceTransitionsJobIntentService
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingEvent
import java.text.SimpleDateFormat
import java.util.*

class GeofenceTransitionsJobIntentService : JobIntentService() {
    /**
     * Handles incoming intents.
     * @param intent sent by Location Services. This Intent is provided to Location
     * Services (inside a PendingIntent) when addGeofences() is called.
     */
    override fun onHandleWork(intent: Intent) {
        val geofencingEvent = GeofencingEvent.fromIntent(intent)
        if (geofencingEvent.hasError()) {
            val errorMessage = getErrorString(
                this,
                geofencingEvent.errorCode
            )
            //            LogUtil.e(TAG, errorMessage);
            return
        }

        // Get the transition type.
        val geofenceTransition = geofencingEvent.geofenceTransition

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER || geofenceTransition == Geofence.GEOFENCE_TRANSITION_DWELL || geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {

            // Get the geofences that were triggered. A single event can trigger multiple geofences.
            val triggeringGeofences = geofencingEvent.triggeringGeofences

            // Get the transition details as a String.
            val geofenceTransitionDetails = getGeofenceTransitionDetails(
                geofenceTransition,
                triggeringGeofences
            )

            //TODO: send enter and exit time to the server
            if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            }

            // Send notification and log the transition details.
            // sendNotification(geofenceTransitionDetails);
        } else {
            // Log the error.
        }
    }

    /**
     * upload dwelling time
     */
    private fun uploadDwellingTime(featureId: String, startTime: Long, endTime: Long) {}

    /**
     * Gets transition details and returns them as a formatted string.
     *
     * @param geofenceTransition    The ID of the geofence transition.
     * @param triggeringGeofences   The geofence(s) triggered.
     * @return                      The transition details formatted as String.
     */
    private fun getGeofenceTransitionDetails(
        geofenceTransition: Int,
        triggeringGeofences: List<Geofence>
    ): String {
        val geofenceTransitionString = getTransitionString(geofenceTransition)

        // Get the Ids of each geofence that was triggered.
        val triggeringGeofencesIdsList = ArrayList<String?>()
        for (geofence in triggeringGeofences) {
            triggeringGeofencesIdsList.add(geofence.requestId)
        }
        val triggeringGeofencesIdsString = TextUtils.join(", ", triggeringGeofencesIdsList)
        val sf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        return geofenceTransitionString + ": " + triggeringGeofencesIdsString + ": " + sf.format(
            Date(System.currentTimeMillis())
        )
    }

    /**
     * Maps geofence transition types to their human-readable equivalents.
     *
     * @param transitionType    A transition type constant defined in Geofence
     * @return                  A String indicating the type of transition
     */
    private fun getTransitionString(transitionType: Int): String {
        return when (transitionType) {
            Geofence.GEOFENCE_TRANSITION_ENTER -> getString(R.string.geofence_transition_entered)
            Geofence.GEOFENCE_TRANSITION_DWELL -> getString(R.string.geofence_transition_dwell)
            Geofence.GEOFENCE_TRANSITION_EXIT -> getString(R.string.geofence_transition_exited)
            else -> getString(R.string.unknown_geofence_transition)
        }
    }

    companion object {
        private const val JOB_ID = 573
        private const val TAG = "GeofenceTransitionsIS"
        private const val CHANNEL_ID = "CHUR_INDOOR"

        /**
         * Convenience method for enqueuing work in to this service.
         */
        fun enqueueWork(context: Context?, intent: Intent?) {
            enqueueWork(
                context!!,
                GeofenceTransitionsJobIntentService::class.java,
                JOB_ID,
                intent!!
            )
        }
    }
}