package com.churkit.base

import android.content.Context
import android.util.Log
import androidx.annotation.UiThread
import androidx.lifecycle.*
import com.churkit.R
import com.churkit.Task.TrackerTask
import com.churkit.model.request.CKPostUserResgistraion
import com.churkit.model.response.CKRegisterUserResponse
import com.churkit.services.ServiceAliveUtils
import com.churkit.utils.CKAppUtill
import com.churkit.utils.CKSessionUtils
import com.churkit.utils.SharedPreferenceUtils
import com.churkit.utils.ToastUtils
import com.churkit.utils.logger.LogUtil
import com.example.mvvmkotlinexample.repository.CKBaseActivityRepository
import com.mapbox.mapboxsdk.Mapbox

object ChurKit {
    val context: Context? = null

    private const val TAG = "ChurKit"
    private var instance: ChurKit? = null

    lateinit var mainActivityViewModel: BaseViewModel
    public var isDebugEnabled = true
    lateinit var mContext: Context;
      fun getInstance(): ChurKit? {
          if (instance == null) {
              instance = ChurKit
          }
          return instance
      }

    @UiThread
    @Synchronized
    fun initialize(context: Context, appID: String, BusinessUserId: String){
        mContext = context;
   //     Mapbox.getInstance(context, "Token")
        //  Save app Secreate share prerceh
        registerUser(context, appID, BusinessUserId)
    }


    fun registerUser(mContext: Context, appID: String, BusinessUserId: String) {
        ChurKit.mContext = mContext;
        if (CKAppUtill.isStringNullOrBlank(appID)) {
            ToastUtils.showShortToast(
                mContext,
                mContext.resources.getString(R.string.ck_error_start_sdk)
            );
        } else if (CKAppUtill.isStringNullOrBlank(BusinessUserId)) {
            ToastUtils.showShortToast(
                mContext,
                mContext.resources.getString(R.string.ck_error_start_sdk)
            );
        } else {
            // perform user registration from the api
           trackUserRegister( appID, BusinessUserId)
        }
    }

    /**
     * first we check uses has authorized person or not
     * If  authorized user then update start your user update
     * @return true if initialized, false if not
     */
    fun start(mContext: Context) {
        ChurKit.mContext = mContext;
        if (isauthorized()) {
            // perform ChurKit SDK inilizetion and Start your sdk background functionality
            OnStartTracking()
        } else {
            ToastUtils.showShortToast(
                mContext,
                mContext.resources.getString(R.string.ck_error_start_sdk)
            );
        }
    }

    /**
     * ChurKit SDK user update has stoped.
     * @return true if initialized, false if not
     */
    fun stop() {
        // perform ChurKit SDK Stop  background functionality.
        OnStopTracking()
    }

    /**
     * Indicates whether the ChurKit has been in debug mode.
     * @return true if initialized, false if not
     */
    fun setIsDebugEnabled(isDebugEnabled: Boolean) {
        this.isDebugEnabled = isDebugEnabled
    }

    /**
     * Sets the Debug mode used by the ChurKit SDK
     *
     * @param isEnabled
     */

    @JvmName("setDebugEnabled1")
    fun setDebugEnabled(isEnabled: Boolean) {
        this.isDebugEnabled = isEnabled
    }

    fun isauthorized(): Boolean {
        val isauthorized = true;
        return isauthorized;
    }

    fun trackUserRegister(appID: String, BusinessUserId: String) {

        val type = android.os.Build.MANUFACTURER + android.os.Build.MODEL
        val mCKPostUserResgistraion = CKPostUserResgistraion(appID, type, BusinessUserId)
        mCKPostUserResgistraion.avatar = "";
        CKBaseActivityRepository.registerUserApiCall(mCKPostUserResgistraion, object : OnRegisterListener {
            override fun onError(errorMessage: String) {
                ToastUtils.showShortToast(
                    ChurKit.mContext,
                    ChurKit.mContext.resources.getString(R.string.ck_error_start_sdk)
                );
            }

            override fun onSuccess(mCKRegisterUserResponse: CKRegisterUserResponse?) {
                if (mCKRegisterUserResponse?.code == 200) {
                    ToastUtils.showShortToast(
                        mContext,
                        mContext.resources.getString(R.string.ck_success_start_sdk)
                    );
                    SharedPreferenceUtils.getInstance(ChurKit.mContext)?.saveString(
                        CKSessionUtils.KEY_SERIAL_ID,
                        mCKRegisterUserResponse.serial.toString()
                    )
                    start(mContext)
                } else {
                    ToastUtils.showShortToast(
                        mContext,
                        ChurKit.mContext.resources.getString(R.string.ck_error_start_sdk)
                    );
                }
            }

        })
    }

    fun getSerialNumber(): String? {
        return SharedPreferenceUtils.getInstance(mContext)?.getString(CKSessionUtils.KEY_SERIAL_ID,"");
    }

    fun getAppSDKInstance(): Context? {
        return mContext;
    }
    interface OnRegisterListener {
        fun onError(errorMessage: String)
        fun onSuccess(mCKRegisterUserResponse: CKRegisterUserResponse?)
    }

    fun OnStartTracking() {
        val tracker = TrackerTask.Builder()
            .build()
        tracker.serialId = getSerialNumber();
        if (ServiceAliveUtils.isServiceAlive("TrackerService")) {
            tracker.stop()
        } else {
            tracker.start()
        }
    }
    fun OnStopTracking() {
        val tracker = TrackerTask.Builder()
            .build()
        tracker.serialId = getSerialNumber();
        if (ServiceAliveUtils.isServiceAlive("TrackerService")) {
            tracker.stop()
        }
    }
}