package com.churkit.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.churkit.model.CKServicesSetterGetter
import com.churkit.model.request.CKPostUserResgistraion
import com.churkit.model.response.CKBeaconlistResponse
import com.churkit.model.response.CKRegisterUserResponse
import com.example.mvvmkotlinexample.repository.CKBaseActivityRepository


class BaseViewModel : ViewModel() {

    var CKServicesLiveData: MutableLiveData<CKRegisterUserResponse>? = null
    var CKServicesBeaconlistLiveData: MutableLiveData<CKBeaconlistResponse>? = null

    fun registerUser(mCKRegisterUserResponse : CKPostUserResgistraion,mOnRegisterListener : ChurKit.OnRegisterListener)  {
         CKBaseActivityRepository.registerUserApiCall(mCKRegisterUserResponse,mOnRegisterListener)
    }
    fun getBeaconList() : LiveData<CKBeaconlistResponse>? {
        CKServicesBeaconlistLiveData = CKBaseActivityRepository.getBeanconApiCall()
        return CKServicesBeaconlistLiveData
    }
}