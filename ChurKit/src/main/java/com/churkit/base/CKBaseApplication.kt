package com.churkit.base

import android.content.Context
import androidx.lifecycle.*
import dagger.android.AndroidInjector
import com.churkit.dagger.component.DaggerCKApplicationComponent
import com.churkit.utils.logger.LogUtil
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import dagger.android.support.DaggerApplication

/**
 * Created by Samyak Jain on 22,Nov,2021
 */
class CKBaseApplication : DaggerApplication(), LifecycleObserver, LifecycleOwner {
    lateinit var context: Context
    var mDatabase: DatabaseReference? = null
    override fun onCreate() {
        super.onCreate()
        context = this
        ProcessLifecycleOwner.get()
            .lifecycle.addObserver(this) // to observe Application lifecycle events
        FirebaseDatabase.getInstance().setPersistenceEnabled(true)
        mDatabase = FirebaseDatabase.getInstance().reference

    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val component = DaggerCKApplicationComponent.builder().application(this).build()
        component.inject(this)
        return component
    }


    override fun getLifecycle(): Lifecycle {
        TODO("Not yet implemented")
    }

}