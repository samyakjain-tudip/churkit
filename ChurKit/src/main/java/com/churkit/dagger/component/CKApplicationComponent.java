package com.churkit.dagger.component;


import android.app.Application;

import com.churkit.base.CKBaseApplication;
import com.churkit.dagger.module.CKActivityBindingModule;
import com.churkit.dagger.module.CKNetworkModule;
import com.churkit.dagger.module.CKContextModule;
import com.churkit.dagger.module.CKFragmentBindingModule;
import com.churkit.dagger.module.CKViewModelModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;

/**
 * Created by Samyak Jain on 22,Nov,2021
 */

@Singleton
@Component(modules = {
        CKContextModule.class,
        CKNetworkModule.class,
        AndroidSupportInjectionModule.class,
        CKFragmentBindingModule.class,
        CKActivityBindingModule.class,
        CKViewModelModule.class})

public interface CKApplicationComponent extends AndroidInjector<DaggerApplication> {

    void inject(CKBaseApplication application);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        CKApplicationComponent build();
    }

}