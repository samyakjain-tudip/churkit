package com.churkit.dagger.module

import com.churkit.ui.list.CKListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Samyak Jain on 22,Nov,2021
 */

@Module
abstract class CKFragmentBindingModule {

    @ContributesAndroidInjector
    abstract fun provideListFragment(): CKListFragment?

}