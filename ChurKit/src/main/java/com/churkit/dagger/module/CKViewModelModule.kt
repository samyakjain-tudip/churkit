package com.churkit.dagger.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.churkit.base.CKViewModelFactory
import com.churkit.dagger.util.CKViewModelKey
import com.churkit.ui.list.CKListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by Samyak Jain on 22,Nov,2021
 */

@Module
abstract class CKViewModelModule {

    @Binds
    @IntoMap
    @CKViewModelKey(value = CKListViewModel::class)
    abstract fun bindListViewModel(CKListViewModel: CKListViewModel?): ViewModel?

    @Binds
    abstract fun bindViewModelFactory(factoryCK: CKViewModelFactory?): ViewModelProvider.Factory?

}