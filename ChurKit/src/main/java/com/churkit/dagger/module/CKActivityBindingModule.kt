package com.churkit.dagger.module

import com.churkit.CKMainActivity
import dagger.Module

import dagger.android.ContributesAndroidInjector

/**
 * Created by Samyak Jain on 22,Nov,2021
 */

@Module
abstract class CKActivityBindingModule {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): CKMainActivity?
}