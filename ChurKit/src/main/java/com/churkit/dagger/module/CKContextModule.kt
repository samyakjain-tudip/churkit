package com.churkit.dagger.module;

import android.app.Application
import android.content.Context

import dagger.Binds
import dagger.Module

/**
 * Created by Samyak Jain on 22,Nov,2021
 */

@Module
abstract class CKContextModule {
    @Binds
    abstract fun provideContext(application: Application?): Context?
}