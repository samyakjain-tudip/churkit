package com.churkit.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.churkit.R
import com.churkit.model.CKRepo
import kotlinx.android.synthetic.main.view_repo_list_item.view.*


/**
 * Created by Samyak Jain on 22,Nov,2021
 */

class CKRepoListAdapter constructor(
    viewModelCK: CKListViewModel,
    lifecycleOwner: LifecycleOwner?) :
    RecyclerView.Adapter<CKRepoListAdapter.RepoViewHolder?>() {

    private val data: MutableList<CKRepo> = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.view_repo_list_item, parent, false)
        return RepoViewHolder(view)
    }

    override fun onBindViewHolder(holder: RepoViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemId(position: Int): Long {
        return data[position].id
    }

    class RepoViewHolder(itemView: View) :

        RecyclerView.ViewHolder(itemView) {

        private var CKRepo: CKRepo? = null

        fun bind(CKRepo: CKRepo) {

            this.CKRepo = CKRepo
            itemView.tv_repo_name!!.text = CKRepo.name
            itemView.tv_repo_description!!.text = CKRepo.description
            itemView.tv_forks!!.text = CKRepo.forks.toString()
            itemView.tv_stars!!.text = CKRepo.stars.toString()
        }

    }

    init {
        viewModelCK.getRepos().observe(lifecycleOwner!!) { repos ->
            data.clear()
            if (repos != null) {
                data.addAll(repos)
                notifyDataSetChanged()
            }
        }
        setHasStableIds(true)
    }
}