package com.churkit.ui.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.churkit.model.CKRepo
import com.churkit.network.CKApiRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


/**
 * Created by Samyak Jain on 22,Nov,2021
 */


class CKListViewModel @Inject constructor(private val repoRepositoryCK: CKApiRepository) : ViewModel() {
    private var disposable: CompositeDisposable?
    private val repos = MutableLiveData<List<CKRepo>>()
    private val repoLoadError = MutableLiveData<Boolean>()
    private val loading = MutableLiveData<Boolean>()
    fun getRepos(): LiveData<List<CKRepo>> {
        return repos
    }

    fun getError(): LiveData<Boolean> {
        return repoLoadError
    }

    fun getLoading(): LiveData<Boolean> {
        return loading
    }

    private fun fetchRepos() {
        loading.value = true

    }

    override fun onCleared() {
        super.onCleared()
        if (disposable != null) {
            disposable!!.clear()
            disposable = null
        }
    }

    init {
        disposable = CompositeDisposable()
        fetchRepos()
    }
}