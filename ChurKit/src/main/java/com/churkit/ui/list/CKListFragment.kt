package com.churkit.ui.list

import android.os.Bundle
import android.view.View
import androidx.annotation.Nullable
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.churkit.R
import com.churkit.base.CKBaseFragment
import com.churkit.base.CKViewModelFactory
import com.churkit.model.CKRepo
import kotlinx.android.synthetic.main.screen_list.*
import javax.inject.Inject


/**
 * Created by Samyak Jain on 22,Nov,2021
 */

class CKListFragment : CKBaseFragment() {

    @Inject
    lateinit var CKViewModelFactory: CKViewModelFactory

    private var viewModelCK: CKListViewModel? = null
    override fun layoutRes(): Int {
        return R.layout.screen_list
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        viewModelCK = ViewModelProvider(activity!!, CKViewModelFactory).get(CKListViewModel::class.java)

        recyclerView!!.adapter = CKRepoListAdapter(viewModelCK!!, this)
        recyclerView!!.layoutManager = LinearLayoutManager(context)
        observableViewModel()
    }


    private fun observableViewModel() {
        viewModelCK!!.getRepos().observe(activity!!,
            { CKRepos: List<CKRepo?>? ->
                if (CKRepos != null) recyclerView!!.visibility = View.VISIBLE
            })

        viewModelCK!!.getError().observe(activity!!,
            { isError: Boolean? ->
                if (isError != null) if (isError) {
                    tv_error!!.visibility = View.VISIBLE
                    recyclerView!!.visibility = View.GONE
                    tv_error!!.text = "An Error Occurred While Loading Data!"
                } else {
                    tv_error!!.visibility = View.GONE
                    tv_error!!.text = null
                }
            })

        viewModelCK!!.getLoading().observe(activity!!,
            { isLoading: Boolean? ->
                if (isLoading != null) {
                    loading_view!!.visibility = if (isLoading) View.VISIBLE else View.GONE
                    if (isLoading) {
                        tv_error!!.visibility = View.GONE
                        recyclerView!!.visibility = View.GONE
                    }
                }
            })
    }
}