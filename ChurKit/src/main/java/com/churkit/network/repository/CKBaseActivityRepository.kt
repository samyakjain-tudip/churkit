package com.example.mvvmkotlinexample.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.churkit.base.ChurKit
import com.churkit.model.request.CKPostUserResgistraion
import com.churkit.model.response.CKBeaconlistResponse
import com.churkit.model.response.CKRegisterUserResponse
import com.example.mvvmkotlinexample.retrofit.CKRetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object CKBaseActivityRepository {

    val serviceSetterGetter = MutableLiveData<CKRegisterUserResponse>()
    val serviceBeaconlist = MutableLiveData<CKBeaconlistResponse>()

    fun registerUserApiCall(mCKPostUserResgistraion : CKPostUserResgistraion,mOnRegisterListener : ChurKit.OnRegisterListener){

        val call = CKRetrofitClient.CK_API_INTERFACE.registerUser(mCKPostUserResgistraion);

        call.enqueue(object: Callback<CKRegisterUserResponse> {
            override fun onFailure(call: Call<CKRegisterUserResponse>, t: Throwable) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", t.message.toString())
                mOnRegisterListener.onError("Api Not Working")
            }

            override fun onResponse(
                call: Call<CKRegisterUserResponse>,
                response: Response<CKRegisterUserResponse>
            ) {
                Log.v("DEBUG : ", response.body().toString())
                val data = response.body()

                val code = data!!.code
                val serial = data!!.serial
                val msg = data!!.msg
                mOnRegisterListener.onSuccess(data)
                serviceSetterGetter.value = CKRegisterUserResponse(code,serial,msg)
            }
        })
    }
    fun getBeanconApiCall(): MutableLiveData<CKBeaconlistResponse> {

        val call = CKRetrofitClient.CK_API_INTERFACE.getListbeacon()

        call.enqueue(object: Callback<CKBeaconlistResponse> {
            override fun onFailure(call: Call<CKBeaconlistResponse>, t: Throwable) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", t.message.toString())
            }

            override fun onResponse(
                call: Call<CKBeaconlistResponse>,
                response: Response<CKBeaconlistResponse>
            ) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", response.body().toString())

                val data = response.body()


            }
        })
        return serviceBeaconlist
    }
}