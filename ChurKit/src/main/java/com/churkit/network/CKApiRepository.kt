package com.churkit.network

import com.churkit.model.CKRepo
import io.reactivex.Observable

import javax.inject.Inject

/**
 * Created by Samyak Jain on 22,Nov,2021
 */

class CKApiRepository @Inject constructor(private val repoService: CKApiServices) {

    fun getRepositories(): Observable<List<CKRepo?>?>? {
        return repoService.getRepositories()
    }

}