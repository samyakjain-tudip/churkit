package com.churkit.network

import com.churkit.model.CKRepo
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * Created by Samyak Jain on 22,Nov,2021
 */

interface CKApiServices {
    @GET("users/afreakyelf/repos")
    fun getRepositories(): Observable<List<CKRepo?>?>?
}