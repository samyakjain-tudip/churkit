package com.example.mvvmkotlinexample.retrofit


import com.churkit.model.CKServicesSetterGetter
import com.churkit.model.request.CKPostUserResgistraion
import com.churkit.model.response.CKBeaconlistResponse
import com.churkit.model.response.CKRegisterUserResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface CKApiInterface {

    @GET("beacons")
    fun getListbeacon() : Call<CKBeaconlistResponse>


    @POST("devices/register")
    fun registerUser(@Body user : CKPostUserResgistraion) : Call<CKRegisterUserResponse>
}