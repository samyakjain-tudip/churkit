package com.churkit.Task

import com.churkit.base.ChurKit.getAppSDKInstance
import com.churkit.services.ServiceAliveUtils.isServiceAlive
import com.churkit.base.ChurKit
import com.churkit.services.ServiceAliveUtils
import android.content.Intent
import com.churkit.services.CKUpdateDataService
import android.annotation.SuppressLint
import android.os.Build
import com.churkit.Task.TrackerTask
import com.churkit.utils.Constants
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class TrackerTask {
    var serialId: String? = null
    private var executorService: ExecutorService? = null
    private val notificationTitleForeground: String? = null
    private val notificationContentForeground: String? = null
    fun start() {
        scheduleTrackerTask()
    }

    fun stop() {
        stopService()
    }

    private fun stopService() {
        val context = getAppSDKInstance()
        if (isServiceAlive("com.churkit.services.CKUpdateDataService")) {
            val intent = Intent(context, CKUpdateDataService::class.java)
            context!!.stopService(intent)
        }
    }

    /**
     * To use worker manager to awaken the service
     */
    @SuppressLint("RestrictedApi")
    private fun scheduleTrackerTask() {
        if (!isServiceAlive("com.churkit.services.CKUpdateDataService")) {
            val intent = Intent(ChurKit.mContext, CKUpdateDataService::class.java)
            intent.putExtra(Constants.NOTIFICATION_TITLE_FOREGROUND, notificationTitleForeground)
            intent.putExtra(
                Constants.NOTIFICATION_CONTENT_FOREGROUND,
                notificationContentForeground
            )
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                ChurKit.mContext.startForegroundService(intent)
            } else {
                ChurKit.mContext.applicationContext.startService(intent)
            }
        }
    }

    private fun setExecutorService(executorService: ExecutorService) {
        this.executorService = executorService
    }
    public fun setSerialID(SerialID: String) {
        this.serialId = SerialID
    }
    class Builder {
        private val executorService: ExecutorService
        private val SerialID: String? = null

        fun build(): TrackerTask {
            val task = TrackerTask()
            task.serialId = SerialID
            task.setExecutorService(executorService)
            return task
        }

        init {
            executorService = Executors.newSingleThreadExecutor()
        }
    }

    companion object {
        private val TAG = TrackerTask::class.java.simpleName
        private const val TRACKER_WORK_NAME = "TRACKER-WORK"
    }
}